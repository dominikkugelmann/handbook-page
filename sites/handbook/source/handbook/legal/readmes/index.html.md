---
layout: handbook-page-toc
title: "GitLab Legal Team READMEs"
description: "Get to know the Legal Team in our individual README pages"
---

## Legal Team READMEs

- [Julie Braughler (Contract Manager)](https://about.gitlab.com/handbook/legal/readmes/juliebraughler.index.html)
- [Rashmi Chachra (Director of Legal, Corporate)](https://about.gitlab.com/handbook/legal/readmes/rashmichachra.index.html)
- [Sarah Chia (Junior Paralegal)](https://about.gitlab.com/handbook/legal/readmes/sarahchia.index.html)
- [Robyn Hartough (Sr. EBA)](https://about.gitlab.com/handbook/legal/readmes/robynhartough.index.html)
- [Rob Nalen (Director of Legal Operations & Contracts)](https://about.gitlab.com/handbook/legal/readmes/robnalen.index.html)
- [Miguel Silva (Sr. Contract Manager)](https://about.gitlab.com/handbook/legal/readmes/miguelsilva.index.html)

